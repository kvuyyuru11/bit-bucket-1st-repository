/**
 * @File Name          : AccountController.cls
 * @Description        :
 * @Author             : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Group              :
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 6/26/2019, 3:12:04 PM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    6/18/2019, 11:35:10 AM   ChangeMeIn@UserSettingsUnder.SFDoc     Initial Version
**/
//abcdefghijkl
public class AccountController{
    @AuraEnabled
    public static List <Account> fetchAccounts() {

        List<Account> accList = [SELECT Id, Name, BillingState,
                                    Website, Phone, Industry, Type
                                    from Account limit 1];

        return accList;
    }
}